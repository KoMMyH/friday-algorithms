
const sequence = Array.from({ length: 100 }, () => {
  return 1 + Math.floor(100 * Math.random());
});

//console.log('Initial sequence: ', sequence);

const findLongestSubsequence = (sequence) => {
  let tempArr = [];
  let result = [];

  for (let i = 0; i < sequence.length; ++i) {
    if (!tempArr.length) {
      tempArr.push(sequence[i])
      continue;
    }

    if (tempArr[tempArr.length - 1] <= sequence[i]) {
      tempArr.push(sequence[i])
    } else {
      if (result.length < tempArr.length) result = tempArr;
      tempArr = [sequence[i]];
    }
  }
  return result
}

console.log('Result: ', findLongestSubsequence([1, 6, 2, 25, 12, 54, 67, 82, 12]));

// [1, 6, 2, 25, 12, 54, 67, 82, 12]