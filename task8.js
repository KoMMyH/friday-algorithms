const canBeCorrectArithmeticExpr = (brackets) => {
  const stack = [];

  for (let i = 0; i < brackets.length; ++i) {
    if (brackets[i] !== '(' && brackets[i] !== ')') return false;

    if (stack.length === 0) {
      if (brackets[i] === '(') {
        stack.push(brackets[i]);
      } else {
        return false;
      }
      continue;
    }

    if (stack[stack.length - 1] === '(' && brackets[i] === ')') {
      stack.pop();
      continue;
    }

    stack.push(brackets[i]);
  }

  return stack.length === 0
}

console.log('canBeCorrectArithmeticExpr("((()())())"): ', canBeCorrectArithmeticExpr("((()())())"));
// canBeCorrectArithmeticExpr("((()())())"): true

console.log('canBeCorrectArithmeticExpr("((()())))"): ', canBeCorrectArithmeticExpr("((()())))"));
// canBeCorrectArithmeticExpr("((()())))"): false