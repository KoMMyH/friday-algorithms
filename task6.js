
const epsilon = Math.pow(10, -5);

const findCrossingPointOfLines = (firstPoints, secondPoints) => {
  // kF = (y2 - y1) / (x2 - x1)
  // bF = y1 - kF * x1

  const kF = (firstPoints.second.y - firstPoints.first.y) / (firstPoints.second.x - firstPoints.first.x);
  const bF = firstPoints.first.y - kF * firstPoints.first.x;
  const kS = (secondPoints.second.y - secondPoints.first.y) / (secondPoints.second.x - secondPoints.first.x);
  const bS = secondPoints.first.y - kS * secondPoints.first.x;

  if (Math.abs(kF - kS) <= epsilon && Math.abs(bF - bS) <= epsilon) return "The same line";

  if (Math.abs(kF - kS) <= epsilon) return "Lines are parallel";

  const x = (bS - bF) / (kF - kS)
  const y = kF * x + bF;

  return { x, y }
}

console.log('findCrossingPointOfLines', findCrossingPointOfLines({
  first: {
    x: 0,
    y: 0,
  },
  second: {
    x: 10,
    y: 10,
  }
}, {
  first: {
    x: 10,
    y: 10,
  },
  second: {
    x: 15,
    y: 15
  }
}))

//Тут значения для проверки выбирайте сами и вводите в том формате, в котором вам будет удобнее (массив или объект)