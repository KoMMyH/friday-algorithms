const isEven = (number) => {
  return !(number & 1);
}

// 6 - 0110
// 7 - 0111
// 8 - 1000

console.log('isEven(6): ', isEven(6));
// isEven(6): true

console.log('isEven(11): ', isEven(11));
// isEven(11): false

const isPowerOfTwo = (number) => {
  return !(number & (number >> 1))
}

console.log('isPowerOfTwo(6): ', isPowerOfTwo(6));
// isPowerOfTwo(6): false

console.log('isPowerOfTwo(16): ', isPowerOfTwo(16));
// isPowerOfTwo(16): true