
const binarySearch = (value, array, startIndex = 0, endIndex = array.length) => {
  if (endIndex - startIndex === 0) return -1;

  if (endIndex - startIndex === 1) {
    return value === array[startIndex] ? startIndex : -1;
  }

  const midIndex = Math.floor((endIndex + startIndex) / 2);
  if (array[midIndex] === value) return midIndex;
  if (value < array[midIndex]) return binarySearch(value, array, startIndex, midIndex);
  if (value > array[midIndex]) return binarySearch(value, array, midIndex, endIndex);
}

console.log('binarySearch(4, [1, 5, 7, 8]): ', binarySearch(4, [1, 5, 7, 8]));
// binarySearch(): -1

console.log('binarySearch(8, [1, 5, 7, 8]): ', binarySearch(5, [1, 4, 5, 7, 8]));
// binarySearch(): 3