const findLongestPalindromeInStr = (str) => {
  const palindromes = [];

  for (let i = 0; i < str.length; ++i) {
    for (let j = i; j < str.length; ++j) {
      const tempStr = str.slice(i, j);
      if (tempStr === tempStr.split("").reverse().join("") && tempStr.length > 1) {
        palindromes.push(tempStr);
      }
    }
  }

  let longPalingr = '';
  for (let i = 0; i < palindromes.length; ++i) {
    if (longPalingr.length < palindromes[i].length) {
      longPalingr = palindromes[i];
    }
  }

  return longPalingr;
}

console.log('findLongestPalindromeInStr(palilapndromordnpe): ', findLongestPalindromeInStr('palilapndromordnpe'));
// findLongestPalindromeInStr(): pndromordnp