const isInTheMatrix = (matrix, matrixToFind) => {
  if (matrixToFind.length > matrix.length) return null;

  const result = {
    i: null,
    j: null,
  }

  for (let i = 0; i < matrix.length; ++i) {
    for (let j = 0; j < matrix[i].length; ++j) {
      if (matrix[i][j] === matrixToFind[0][0]) {
        let s = 0;
        for (let k = 0; k < matrixToFind.length; ++k) {
          for (let z = 0; z < matrixToFind[k].length; ++z) {
            if (matrix[i + k][j + z] === matrixToFind[k][z]) {
              ++s;
            }
          }
          if (s === matrixToFind.length * matrixToFind.length) {
            result.i = i;
            result.j = j;
          }
        }
      }
    }
  }

  return result;
}

const testMatrix1 = [
  [1, 2, 3, 4, 5],
  [1, 1, 1, 1, 1],
  [4, 3, 2, 3, 2],
  [5, 4, 3, 3, 2],
]

const testMatrix2 = [
  [1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1],
  [1, 1, 1, 1, 1],
]

const matrixToFind = [
  [1, 1, 1, 1],
  [3, 2, 3, 2]
]

console.log('isInTheMatrix(testMatrix1, matrixToFind): ', isInTheMatrix(testMatrix1, matrixToFind));
// isInTheMatrix(testMatrix1, matrixToFind): [1, 1]

// console.log('isInTheMatrix(testMatrix2, matrixToFind): ', isInTheMatrix(testMatrix2, matrixToFind));
// isInTheMatrix(testMatrix2, matrixToFind): null