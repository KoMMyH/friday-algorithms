const nthFiboNumber = (number) => {
  const cache = [0, 1];

  for (let i = 1; i <= number; ++i) {
    cache[i%2] = cache[0] + cache[1];
  }

  return cache[number%2];
}

console.log('nthFiboNumber(4): ', nthFiboNumber(4));
// nthFiboNumber(4): 3

console.log('nthFiboNumber(8): ', nthFiboNumber(8));
// nthFiboNumber(8): 21